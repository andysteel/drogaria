package com.gmail.andersoninfonet.drogaria.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Estado;

public class EstadoDAOTest {

	@Test
	@Ignore
	public void salvar(){
		Estado estado = new Estado();
		estado.setNome("Minas Gerais");
		estado.setSigla("MG");
		
		EstadoDAO dao = new EstadoDAO();
		dao.salvar(estado);
	}
	
	@Test
	public void listar(){
		EstadoDAO dao = new EstadoDAO();
		
		List<Estado> resultado = dao.listar();
		
		for(Estado estado : resultado){
			System.out.println("Codigo: "+estado.getCodigo());
			System.out.println("Estado: "+estado.getNome());
			System.out.println("Sigla: "+estado.getSigla());
			System.out.println();
		}
	}
	
	@Test
	@Ignore
	public void buscar(){
		Long codigo = 50L;
		EstadoDAO dao = new EstadoDAO();
		Estado estado = dao.buscar(codigo);
		
		if(estado == null){
			System.out.println("Nenhum registro encontrado.");
		}else{
			System.out.println("Estado: "+estado.getNome());
		}
	}
	
	@Test
	@Ignore
	public void excluir(){
		Long codigo = 150L;
		EstadoDAO dao = new EstadoDAO();
		Estado estado = dao.buscar(codigo);
		
		if(estado == null){
			System.out.println("Nenhum registro encontrado.");
		}else{
			dao.excluir(estado);
		}
	}
	
	@Test
	@Ignore
	public void editar(){
		Long codigo = 55L;
		EstadoDAO dao = new EstadoDAO();
		Estado estado = dao.buscar(codigo);
		
		if(estado == null){
			System.out.println("Nenhum registro encontrado.");
		}else{
			estado.setNome("São Paulinho");
			dao.editar(estado);
		}
	}
}
