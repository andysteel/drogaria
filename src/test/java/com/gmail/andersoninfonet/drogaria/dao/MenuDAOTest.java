package com.gmail.andersoninfonet.drogaria.dao;

import java.util.List;

import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Menu;

public class MenuDAOTest {

	@Test
	public void listar() {
		MenuDAO  dao = new MenuDAO();
		List<Menu> menus = dao.listar();
		
		for(Menu menu : menus) {
			if (menu.getCaminho() == null) {
				System.out.println(menu.getLabel() + " - "+menu.getCaminho());
				for(Menu item : menu.getMenus()) {
					System.out.println("\t"+ item.getLabel() + " - "+item.getCaminho());
				}
			}
		}
	}
}
