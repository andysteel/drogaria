package com.gmail.andersoninfonet.drogaria.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Caixa;

public class CaixaDAOTest {

	@Ignore
	@Test
	public void salvar() throws ParseException{
		Caixa caixa = new Caixa();
		caixa.setDataAbertura(new SimpleDateFormat("dd/MM/yyyy").parse("23/04/2016"));
		caixa.setValorAbertura(new BigDecimal("100.00"));
		CaixaDAO dao = new CaixaDAO();
		dao.salvar(caixa);
	}
	
	@Test
	public void buscar() throws ParseException{
		CaixaDAO dao = new CaixaDAO();
		Caixa caixa = new Caixa();
		caixa = dao.buscar(new SimpleDateFormat("dd/MM/yyyy").parse("23/04/2016"));
		Assert.assertNotNull(caixa);
	}
	
}
