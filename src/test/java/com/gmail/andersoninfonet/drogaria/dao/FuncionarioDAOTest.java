package com.gmail.andersoninfonet.drogaria.dao;

import java.util.Date;

import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Funcionario;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;

public class FuncionarioDAOTest {

	@Test
	public void salvar(){
		Funcionario funcionario = new Funcionario();
		Pessoa pessoa = new Pessoa();
		PessoaDAO pessoaDAO = new PessoaDAO();
		pessoa = pessoaDAO.buscar(152L);
		
		funcionario.setPessoa(pessoa);
		funcionario.setDataAdmissao(new Date());
		funcionario.setCarteiraTrabalho("123548796541235");
		FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
		funcionarioDAO.merge(funcionario);
		
	}
}
