package com.gmail.andersoninfonet.drogaria.dao;

import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Pessoa;

public class PessoaDAOTest {

	@Test
	public void salvar(){
		Pessoa pessoa = new Pessoa();
		pessoa.setBairro("Aldeia da Prata");
		pessoa.setCelular("92364439");
		pessoa.setCep("24858004");
		pessoa.setComplemento("lote 25");
		pessoa.setCpf("09963009700");
		pessoa.setEmail("binha@gmail.com");
		pessoa.setNome("Nubia da Conceição");
		pessoa.setNumero(new Short("220"));
		pessoa.setRg("0130326184");
		pessoa.setRua("Estrada Ademar Ferreira Torres");
		pessoa.setTelefone("36380119");
		
		PessoaDAO pessoaDAO = new PessoaDAO();
		pessoaDAO.salvar(pessoa);
		System.out.println("pessoa salvo com sucesso !!");
	}
}
