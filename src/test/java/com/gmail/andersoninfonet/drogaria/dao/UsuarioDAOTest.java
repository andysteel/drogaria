package com.gmail.andersoninfonet.drogaria.dao;

import static com.gmail.andersoninfonet.drogaria.enumm.TipoUsuario.ADMINISTRADOR;
import static com.gmail.andersoninfonet.drogaria.enumm.TipoUsuario.BALCONISTA;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.Ignore;
import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.enumm.TipoUsuario;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;
import com.gmail.andersoninfonet.drogaria.model.Usuario;

public class UsuarioDAOTest {

	@Test
	public void salvar(){
		Usuario usuario = new Usuario();
		usuario.setAtivo(true);
		PessoaDAO pessoaDAO = new PessoaDAO();
		Pessoa pessoa = pessoaDAO.buscar(152L);
		System.out.println("pessoa encontrada: "+pessoa.getNome());
		System.out.println("teste");
		usuario.setPessoa(pessoa);
		usuario.setSenhaSemCriptografia("q1w2e3r4");
		
		SimpleHash hash = new SimpleHash("md5", usuario.getSenhaSemCriptografia());
		usuario.setSenha(hash.toHex());
		usuario.setTipoUsuario(BALCONISTA);
		
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		usuarioDAO.salvar(usuario);
		System.out.println("Usuario salvo com sucesso !!");
	}
	
	@Ignore
	@Test
	public void autenticar(){
		String cpf = "099.630.097-00";
		String senha = "q1w2e3r";
		
		UsuarioDAO dao = new UsuarioDAO();
		Usuario usuario = dao.autenticar(cpf, senha);
		
		if(usuario == null){
			System.out.println("usuario não encontrado !!!");
		}else{
			System.out.println("usuario encontrado !! "+usuario.getPessoa().getNome());
		}
	}
}
