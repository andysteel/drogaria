package com.gmail.andersoninfonet.drogaria.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Cidade;
import com.gmail.andersoninfonet.drogaria.model.Estado;

public class CidadeDAOTest {

	@Test
	@Ignore
	public void salvar(){
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = estadoDAO.buscar(300L);
		
		Cidade cidade = new Cidade();
		cidade.setNome("Belo Horizonte");
		cidade.setEstado(estado);
		
		CidadeDAO cidadeDAO = new CidadeDAO();
		cidadeDAO.salvar(cidade);
	}
	
	@Test
	@Ignore
	public void listar(){
		CidadeDAO cidadeDAO = new CidadeDAO();
		List<Cidade> resultado = cidadeDAO.listar();
		
		for(Cidade cidade : resultado){
			System.out.println("codigo :"+cidade.getCodigo());
			System.out.println("cidade :"+cidade.getNome());
			System.out.println("Estado :"+cidade.getEstado().getNome());
			System.out.println();
		}
	}
	
	@Test
	@Ignore
	public void buscar(){
		CidadeDAO cidadeDAO = new CidadeDAO();
		Cidade cidade = cidadeDAO.buscar(200L);
		
		System.out.println("codigo :"+cidade.getCodigo());
		System.out.println("cidade :"+cidade.getNome());
		System.out.println("Estado :"+cidade.getEstado().getNome());
	}
	
	@Test
	@Ignore
	public void excluir(){
		CidadeDAO cidadeDAO = new CidadeDAO();
		Cidade cidade = cidadeDAO.buscar(200L);
		
		cidadeDAO.excluir(cidade);
		
		System.out.println("cidade removida: "+cidade.getNome());
	}
	
	@Test
	@Ignore
	public void editar(){
		CidadeDAO cidadeDAO = new CidadeDAO();
		Cidade cidade = cidadeDAO.buscar(650L);
		
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = estadoDAO.buscar(250L);
		System.out.println("estado :"+estado.getNome());
		System.out.println();
		
		System.out.println("codigo :"+cidade.getCodigo());
		System.out.println("cidade :"+cidade.getNome());
		System.out.println("Estado :"+cidade.getEstado().getNome());
		System.out.println();
		
		cidade.setNome("Rio Bonito");
		cidade.setEstado(estado);
		
		cidadeDAO.editar(cidade);
		
		System.out.println("codigo :"+cidade.getCodigo());
		System.out.println("cidade :"+cidade.getNome());
		System.out.println("Estado :"+cidade.getEstado().getNome());
	}
	
	@Test
	public void buscarPorEstado(){
		Long codigo = 50L;
		CidadeDAO cidadeDAO = new CidadeDAO();
		List<Cidade> resultado = cidadeDAO.buscarPorEstado(codigo);
		
		for(Cidade cidade : resultado){
			System.out.println("codigo :"+cidade.getCodigo());
			System.out.println("cidade :"+cidade.getNome());
			System.out.println("Estado :"+cidade.getEstado().getNome());
			System.out.println();
		}
	}
}
