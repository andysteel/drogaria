package com.gmail.andersoninfonet.drogaria.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Cliente;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;

public class ClienteDAOTest {

	@Test
	public void salvar() throws ParseException{
		PessoaDAO pessoaDAO = new PessoaDAO();
		Pessoa pessoa = pessoaDAO.buscar(900L);
		
		Cliente cliente = new Cliente();
		cliente.setDataCadastro(new SimpleDateFormat("dd/MM/yyyy").parse("02/12/2015"));
		cliente.setLiberado(false);
		cliente.setPessoa(pessoa);
		
		ClienteDAO clienteDAO = new ClienteDAO();
		clienteDAO.salvar(cliente);
		
		System.out.println("cliente salvo com sucesso !!");
	}
}
