package com.gmail.andersoninfonet.drogaria.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Fabricante;

public class FabricanteDAOTest {

	@Test
	@Ignore
	public void salvar(){
		Fabricante fabricante = new Fabricante();
		fabricante.setDescricao("Medley");
		
		FabricanteDAO dao = new FabricanteDAO();
		dao.salvar(fabricante);
	}
	
	@Test
	@Ignore
	public void listar(){
		FabricanteDAO dao = new FabricanteDAO();
		
		List<Fabricante> resultado = dao.listar();
		
		for(Fabricante fabricante : resultado){
			System.out.println("Fabricante: "+fabricante.getDescricao());
		}
	}
	
	@Test
	public void mergeIncluir(){
		Fabricante fabricante = new Fabricante();
		fabricante.setDescricao("EMS-TESTE");
		
		FabricanteDAO dao = new FabricanteDAO();
		dao.merge(fabricante);
	}
	
	@Test
	@Ignore
	public void mergeEditar(){
		FabricanteDAO dao = new FabricanteDAO();
		Fabricante fabricante = dao.buscar(1100L);
		fabricante.setDescricao("EMS");
		
		dao.merge(fabricante);
	}
}
