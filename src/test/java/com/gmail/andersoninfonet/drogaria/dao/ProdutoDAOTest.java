package com.gmail.andersoninfonet.drogaria.dao;

import java.math.BigDecimal;

import org.junit.Test;

import com.gmail.andersoninfonet.drogaria.model.Fabricante;
import com.gmail.andersoninfonet.drogaria.model.Produto;

public class ProdutoDAOTest {

	@Test
	public void salvar(){
		FabricanteDAO fabricanteDAO = new FabricanteDAO();
		Fabricante fabricante = fabricanteDAO.buscar(100L);
		
		Produto produto = new Produto();
		produto.setDescricao("cataflan 50mg");
		produto.setFabricante(fabricante);
		produto.setPreco(new BigDecimal("13.70"));
		produto.setQuantidade(new Short("7"));
		
		ProdutoDAO produtoDAO = new ProdutoDAO();
		produtoDAO.salvar(produto);
		System.out.println("produto salvo com sucesso !!");
	}
}
