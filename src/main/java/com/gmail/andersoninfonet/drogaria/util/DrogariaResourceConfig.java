package com.gmail.andersoninfonet.drogaria.util;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
//localhost:8080/drogaria/rest
@ApplicationPath("rest")
public class DrogariaResourceConfig extends ResourceConfig{

	public DrogariaResourceConfig(){
		packages("com.gmail.andersoninfonet.drogaria.webservices");
	}
}
