package com.gmail.andersoninfonet.drogaria.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.gmail.andersoninfonet.drogaria.dao.FabricanteDAO;
import com.gmail.andersoninfonet.drogaria.dao.ProdutoDAO;
import com.gmail.andersoninfonet.drogaria.model.Fabricante;
import com.gmail.andersoninfonet.drogaria.model.Produto;
import com.gmail.andersoninfonet.drogaria.util.HibernateUtil;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;

@ManagedBean
@ViewScoped
public class ProdutoBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Produto produto;
	private List<Produto> produtos;
	private List<Fabricante> fabricantes;
	private StreamedContent foto;
	
	@PostConstruct
	public void listar() {
		try {
			ProdutoDAO produtoDAO = new ProdutoDAO();
			produtos = produtoDAO.listar("descricao");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os produtos");
			erro.printStackTrace();
		}
	}
	
	public void novo(){
		try {
			produto = new Produto();

			FabricanteDAO fabricanteDAO = new FabricanteDAO();
			fabricantes = fabricanteDAO.listar("descricao");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar gerar um novo produto");
			erro.printStackTrace();
		}
	}
	
	public void excluir(ActionEvent evento){
		try {
			produto = (Produto) evento.getComponent().getAttributes().get("produtoSelecionado");

			ProdutoDAO produtoDAO = new ProdutoDAO();
			produtoDAO.excluir(produto);

			Path arquivo = Paths.get("/home/andysteel/upload/"+"origem"+produto.getCodigo()+".png");
			Files.deleteIfExists(arquivo);
			
			listar();

			Messages.addGlobalInfo("Produto removido com sucesso");
		} catch (RuntimeException | IOException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar remover o produto");
			erro.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		try {
			produto = (Produto) evento.getComponent().getAttributes().get("produtoSelecionado");
			produto.setCaminho("/home/andysteel/upload/"+"origem"+produto.getCodigo()+".png");
			
			FabricanteDAO fabricanteDAO = new FabricanteDAO();
			fabricantes = fabricanteDAO.listar();
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar selecionar um produto");
			erro.printStackTrace();
		}	
	}
	
	public void salvar(){
		try {
			if(produto.getCaminho() == null){
				Messages.addGlobalError("O campo Foto é obrigatório !");
				return;
			}
			
			ProdutoDAO produtoDAO = new ProdutoDAO();
			Produto produtoRetorno =  produtoDAO.merge(produto);
			Path origem = Paths.get(produto.getCaminho());
			Path destino = Paths.get("/home/andysteel/upload/"+"origem"+produtoRetorno.getCodigo()+".png");
			Files.copy(origem, destino, StandardCopyOption.REPLACE_EXISTING);
			
			novo();
			listar();

			Messages.addGlobalInfo("Produto salvo com sucesso");
		} catch (RuntimeException | IOException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o produto");
			erro.printStackTrace();
		}
	}
	
	public void upload(FileUploadEvent evento){
		
		try {
			UploadedFile arquivoUpload =  evento.getFile();
			Path arquivoTemp =  Files.createTempFile(null, null);
			Files.copy(arquivoUpload.getInputstream(), arquivoTemp, StandardCopyOption.REPLACE_EXISTING);
			produto.setCaminho(arquivoTemp.toString());
			
			Messages.addGlobalInfo("Upload realizado com sucesso");
		} catch (IOException e) {
			Messages.addGlobalError("Ocorreu um erro ao tentar fazer o upload de arquivo !");
			e.printStackTrace();
		}
	}
	
	public void download(ActionEvent evento){
		 
		try {
			produto = (Produto) evento.getComponent().getAttributes().get("produtoSelecionado");
			InputStream stream = new FileInputStream("/home/andysteel/upload/origem"+produto.getCodigo()+".png");
			foto = new DefaultStreamedContent(stream,"image/png",produto.getCodigo()+".png");
		} catch (FileNotFoundException e) {
			Messages.addGlobalError("Ocorreu um erro ao tentar fazer download !");
			e.printStackTrace();
		}
		
	}
	
	public void imprimir(){
		try{
			DataTable tabela = (DataTable) Faces.getViewRoot().findComponent("formListagem:tabela");
			Map<String, Object> filtros =  tabela.getFilters();
			String produto = (String) filtros.get("descricao");
			String fabricante = (String) filtros.get("fabricante.descricao");
			
			String caminho = Faces.getRealPath("/reports/cadastroDeProdutos.jasper");
			String caminhoBanner = Faces.getRealPath("/resources/images/banner.jpg");
			
			Map<String, Object> parametros = new HashMap<>();
			
			parametros.put("CAMINHO_BANNER", caminhoBanner);
			if(produto == null){
				parametros.put("PRODUTO_DESCRICAO", "%%");
			}else{
				parametros.put("PRODUTO_DESCRICAO", "%"+produto+"%");
			}
			if(fabricante == null){
				parametros.put("FABRICANTE_DESCRICAO", "%%");
			}else{
				parametros.put("FABRICANTE_DESCRICAO", "%"+fabricante+"%");
			}
			
			Connection conn = HibernateUtil.getConexao();
			
			JasperPrint relatorio =  JasperFillManager.fillReport(caminho, parametros, conn);
			JasperPrintManager.printReport(relatorio, true);
		}catch(RuntimeException | JRException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar gerar relatório !");
			e.printStackTrace();
		}
	}
	
	public Produto getProduto() {
		return produto;
	}
	
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}
	
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	
	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}
	
	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}
	
	public StreamedContent getFoto() {
		return foto;
	}
	
	public void setFoto(StreamedContent foto) {
		this.foto = foto;
	}
}
