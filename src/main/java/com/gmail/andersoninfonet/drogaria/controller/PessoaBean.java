package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.CidadeDAO;
import com.gmail.andersoninfonet.drogaria.dao.EstadoDAO;
import com.gmail.andersoninfonet.drogaria.dao.PessoaDAO;
import com.gmail.andersoninfonet.drogaria.model.Cidade;
import com.gmail.andersoninfonet.drogaria.model.Estado;
import com.gmail.andersoninfonet.drogaria.model.Fabricante;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;
import com.google.gson.Gson;

@ManagedBean
@ViewScoped
public class PessoaBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Pessoa pessoa;
	private Estado estado;
	private List<Pessoa> pessoas;
	private List<Estado> estados;
	private List<Cidade> cidades;
	private PessoaDAO dao;
	
	public void novo(){
		try{
			pessoa = new Pessoa();
			estado = new Estado();

			Client cliente = ClientBuilder.newClient();
			WebTarget caminho = cliente.target("http://127.0.0.1:8080/drogaria/rest/estado");
			String json = caminho.request().get(String.class);
			
			Gson gson = new Gson();
			Estado[] vetor = gson.fromJson(json, Estado[].class);
			
			estados = Arrays.asList(vetor);
			cidades = new ArrayList<Cidade>();
			
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar gerar uma nova pessoa");
			e.printStackTrace();
		}
		
	}
	
	@PostConstruct
	public void listar(){
		try{
			dao = new PessoaDAO();
			pessoas = dao.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar listar pessoas");
			e.printStackTrace();
		}
		
	}
	
	public void salvar(){
		try{
			dao = new PessoaDAO();
			dao.merge(pessoa);
			listar();
			Messages.addGlobalInfo("Pessoa "+pessoa.getNome()+" salva com sucesso !");
			novo();
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar salvar uma pessoa");
			e.printStackTrace();
		}
	}
	
	public void excluir(ActionEvent evento){
		try{
			pessoa = (Pessoa) evento.getComponent().getAttributes().get("pessoaSelecionada");
			dao = new PessoaDAO();
			dao.excluir(pessoa);
			Messages.addGlobalInfo("Pessoa "+pessoa.getNome()+" removida com sucesso !");
			listar();
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar remover pessoa !");
			e.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		try{
			pessoa = (Pessoa) evento.getComponent().getAttributes().get("pessoaSelecionada");
			EstadoDAO estadoDAO = new EstadoDAO();
			estados = estadoDAO.listar("nome");
			estado = pessoa.getCidade().getEstado();
			
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidades = cidadeDAO.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao selecionar uma pessoa");
			e.printStackTrace();
		}
		
	}
	
	public void popular(){
		
		try{
			if(estado != null){
				Client cliente = ClientBuilder.newClient();
				WebTarget caminho = cliente.target("http://127.0.0.1:8080/drogaria/rest/cidade/{estadoCodigo}").resolveTemplate("estadoCodigo", estado.getCodigo());
				String json = caminho.request().get(String.class);
				
				Gson gson = new Gson();
				Cidade[] vetor = gson.fromJson(json, Cidade[].class);
				
				cidades = Arrays.asList(vetor);
			}else{
				cidades = new ArrayList<Cidade>();
			}
			
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao filtrar as cidades");
			e.printStackTrace();
		}
		
	}
	
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public Estado getEstado() {
		return estado;
	}
	
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	public List<Pessoa> getPessoas() {
		return pessoas;
	}
	
	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
	
	public List<Cidade> getCidades() {
		return cidades;
	}
	
	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
	
	public List<Estado> getEstados() {
		return estados;
	}
	
	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}
}
