package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.model.Fabricante;
import com.google.gson.Gson;

@ManagedBean
@ViewScoped
public class FabricanteBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Fabricante fabricante;
	private List<Fabricante> fabricantes;
	
	public void novo() {
		fabricante = new Fabricante();
	}
	
	@PostConstruct
	public void listar() {
		try {
			
			Client cliente = ClientBuilder.newClient();
			WebTarget caminho = cliente.target("http://127.0.0.1:8080/drogaria/rest/fabricante");
			String json = caminho.request().get(String.class);
			
			Gson gson = new Gson();
			Fabricante[] vetor = gson.fromJson(json, Fabricante[].class);
			
			fabricantes = Arrays.asList(vetor);
 		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os fabricantes");
			erro.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		fabricante = (Fabricante) evento.getComponent().getAttributes().get("fabricanteSelecionado");
	}
	
	public void excluir(ActionEvent evento){
		try{
			fabricante = (Fabricante) evento.getComponent().getAttributes().get("fabricanteSelecionado");
			String codigo = String.valueOf(fabricante.getCodigo());
			
			Client cliente = ClientBuilder.newClient();
			WebTarget caminho = cliente.target("http://127.0.0.1:8080/drogaria/rest/fabricante").path(codigo);
			caminho.request().delete();
			
			listar();
			Messages.addGlobalInfo("Fabricante removido com sucesso !");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar excluir um fabricante :"+e.getCause());
			e.printStackTrace();
		}
	}
	
	public void salvar(){
		try{
			Client cliente = ClientBuilder.newClient();
			WebTarget caminho = cliente.target("http://127.0.0.1:8080/drogaria/rest/fabricante");
			
			Gson gson = new Gson();
			String json = gson.toJson(fabricante);
			
			//Teste para insert ou update (se o codigo for diferente de null chama o update)
			if(fabricante.getCodigo() == null){
				caminho.request().post(Entity.json(json));
			}else{
				caminho.request().put(Entity.json(json));
			}
			
			novo();
			listar();
			
			Messages.addGlobalInfo("Fabricante salvo com sucesso !");
		}catch(RuntimeException erro){
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar um fabricante :"+erro.getCause());
			erro.printStackTrace();
		}
	}
	
	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

}
