package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.ProdutoDAO;
import com.gmail.andersoninfonet.drogaria.model.Produto;

@ManagedBean
@ViewScoped
public class ProdutoPesquisaBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Produto produto;
	private ProdutoDAO produtoDAO;
	private Boolean exibePainel;
	
	@PostConstruct
	public void novo(){
		produto = new Produto();
		exibePainel = false;
	}
	
	public void buscar(){
		try{
			produtoDAO = new ProdutoDAO();
			Produto resultado = produtoDAO.buscar(produto.getCodigo());
			
			if(resultado == null){
				Messages.addGlobalWarn("Produto não encontrado para o código informado");
				exibePainel = false;
			}else{
				produto = resultado;
				exibePainel = true;
			}
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao buscar o produto !");
			e.printStackTrace();
		}
	}
	
	public Produto getProduto() {
		return produto;
	}
	
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public Boolean getExibePainel() {
		return exibePainel;
	}
	
}
