package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.PessoaDAO;
import com.gmail.andersoninfonet.drogaria.dao.UsuarioDAO;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;
import com.gmail.andersoninfonet.drogaria.model.Usuario;

@ManagedBean
@ViewScoped
public class UsuarioBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Usuario usuario;
	private List<Usuario> usuarios;
	private List<Pessoa> pessoas;
	private UsuarioDAO dao;
	
	public void novo(){
		try{
			usuario = new Usuario();
			
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar criar novo usuário !!");
			e.printStackTrace();
		}
		
		
	}
	
	@PostConstruct
	public void listar(){
		try{
			dao = new UsuarioDAO();
			usuarios = dao.listar();
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar listar usuarios !");
			e.printStackTrace();
		}
		
	}
	
	public void salvar(){
		try{
			dao = new UsuarioDAO();
			dao.merge(usuario);
			novo();
			listar();
			Messages.addGlobalInfo(Faces.getResourceBundle("lbl").getString("usuarioSalvarSucesso"));
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar salvar usuario !!");
			e.printStackTrace();
		}
	}
	
	public void excluir(ActionEvent evento){
		try{
			usuario = (Usuario) evento.getComponent().getAttributes().get("usuarioSelecionado");
			dao = new UsuarioDAO();
			dao.excluir(usuario);
			listar();
			Messages.addGlobalInfo("Usuário excluido com sucesso !!");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar excluir usuario !!");
			e.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		usuario = (Usuario) evento.getComponent().getAttributes().get("usuarioSelecionado");
		try{
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao listar pessoas !!");
			e.printStackTrace();
		}
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public List<Pessoa> getPessoas() {
		return pessoas;
	}
	
	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
}
