package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.EstadoDAO;
import com.gmail.andersoninfonet.drogaria.model.Estado;



@ManagedBean
@ViewScoped
public class EstadoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private Estado estado;
	private List<Estado> estados;
	
	@PostConstruct
	public void listar(){
		try{
			EstadoDAO dao = new EstadoDAO();
			estados = dao.listar();
			
		}catch(RuntimeException e){
			Messages.addGlobalError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void novo(){
		estado = new Estado();
	}
	
	public void salvar(){
		try{
			EstadoDAO dao = new EstadoDAO();
			dao.merge(estado);
			Messages.addGlobalInfo("Estado: "+estado.getNome()+" salvo com sucesso !");
			novo();
			listar();
		}catch(RuntimeException e){
			Messages.addGlobalError(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public void excluir(ActionEvent evento){
		try{
			EstadoDAO dao = new EstadoDAO();
			estado = (Estado) evento.getComponent().getAttributes().get("estadoSelecionado");
			dao.excluir(estado);
			listar();
			Messages.addGlobalInfo("Estado: "+estado.getNome()+" removido com sucesso !");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao remover o estado !");
			e.printStackTrace();
		}
		
	}
	
	public void editar(ActionEvent evento){
		
		estado = (Estado) evento.getComponent().getAttributes().get("estadoSelecionado");
		
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	public List<Estado> getEstados() {
		return estados;
	}
	
	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}
}
