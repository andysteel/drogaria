package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.ClienteDAO;
import com.gmail.andersoninfonet.drogaria.dao.PessoaDAO;
import com.gmail.andersoninfonet.drogaria.model.Cliente;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;

@ManagedBean
@ViewScoped
public class ClienteBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Cliente cliente;
	private List<Cliente> clientes;
	private List<Pessoa> pessoas;
	private ClienteDAO dao;
	
	public void novo(){
		cliente = new Cliente();
		try{
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoas = pessoaDAO.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar criar um novo cliente");
			e.printStackTrace();
		}
	}
	
	@PostConstruct
	public void listar(){
		try{
			dao = new ClienteDAO();
			clientes = dao.listar("dataCadastro");
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar listar clientes !");
			e.printStackTrace();
		}
	}
	
	public void salvar(){
		try{
			dao = new ClienteDAO();
			dao.merge(cliente);
			Messages.addGlobalInfo("Cliente "+cliente.getPessoa().getNome()+" salvo com sucesso");
			novo();
			listar();
		}catch(RuntimeException e){
			Messages.addGlobalError("Erro ao tentar salvar o cliente");
			e.printStackTrace();
		}
	}
	
	public void excluir(){
		
	}
	
	public void editar(){
		
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public List<Cliente> getClientes() {
		return clientes;
	}
	
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public List<Pessoa> getPessoas() {
		return pessoas;
	}
	
	public void setPessoas(List<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}
}
