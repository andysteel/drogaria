package com.gmail.andersoninfonet.drogaria.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.UsuarioDAO;
import com.gmail.andersoninfonet.drogaria.model.Pessoa;
import com.gmail.andersoninfonet.drogaria.model.Usuario;

@ManagedBean
@SessionScoped
public class AutenticacaoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Usuario usuario;
	private Usuario usuarioLogado;
	
	@PostConstruct
	public void iniciar(){
		usuario = new Usuario();
		usuario.setPessoa(new Pessoa());
	}
	
	public void autenticar(){
		try {
			UsuarioDAO dao = new UsuarioDAO();
			usuarioLogado = dao.autenticar(usuario.getPessoa().getCpf(), usuario.getSenha());
			if(usuarioLogado == null){
				Messages.addGlobalError("CPF e/ou senha incorretos");
				return;
			}
			Faces.redirect("./pages/index.xhtml");
		} catch (IOException e) {
			Messages.addGlobalError(e.getCause().toString());
			e.printStackTrace();
		}
	}
	
	public boolean temPermissoes(List<String> permissoes){
		for(String permissao : permissoes){
			//atributo tipo do usuario é Character por isso não pode usar equals para comparar
//			if(usuarioLogado.getTipoUsuario().toString().equals(permissao)){
//				return true;
//			}
		}
		
		return false;
	}
	
	public void logout(){
		Faces.getSession().invalidate();
		try {
			Faces.redirect("./pages/login.xhtml");
		} catch (IOException e) {
			Messages.addGlobalError(e.getCause().toString());
			e.printStackTrace();
		}
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}
}
