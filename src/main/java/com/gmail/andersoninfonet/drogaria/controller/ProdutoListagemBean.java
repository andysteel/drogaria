package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.FabricanteDAO;
import com.gmail.andersoninfonet.drogaria.dao.ProdutoDAO;
import com.gmail.andersoninfonet.drogaria.model.Fabricante;
import com.gmail.andersoninfonet.drogaria.model.Produto;

@ManagedBean
@ViewScoped
public class ProdutoListagemBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Produto produto;
	private List<Produto> produtos;
	private ProdutoDAO produtoDAO;
	private Long codigoProduto;
	private List<Fabricante> fabricantes;
	private FabricanteDAO fabricanteDAO;
	
	@PostConstruct
	public void iniciar(){
		produtoDAO = new ProdutoDAO();
		fabricanteDAO = new FabricanteDAO();
	}
	
	public void listar(){
		try {
			produtos = produtoDAO.listar("descricao");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os produtos");
			erro.printStackTrace();
		}
	}
	
	public void listarFabricantes(){
		try {
			produto = produtoDAO.buscar(codigoProduto);
			fabricantes = fabricanteDAO.listar("descricao");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar fabricantes");
			erro.printStackTrace();
		}
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}
	
	public Produto getProduto() {
		return produto;
	}
	
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}
	
	public Long getCodigoProduto() {
		return codigoProduto;
	}
	
	public void setCodigoProduto(Long codigoProduto) {
		this.codigoProduto = codigoProduto;
	}
}
