package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.CidadeDAO;
import com.gmail.andersoninfonet.drogaria.dao.EstadoDAO;
import com.gmail.andersoninfonet.drogaria.model.Cidade;
import com.gmail.andersoninfonet.drogaria.model.Estado;

@ManagedBean
@ViewScoped
public class CidadeBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<Cidade> cidades;
	private Cidade cidade;
	private List<Estado> estados;
	
	@PostConstruct
	public void listar(){
		try{
			CidadeDAO dao = new CidadeDAO();
			cidades = dao.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar listar as cidades");
			e.printStackTrace();
		}
	}
	
	public void novo(){
		cidade = new Cidade();
		try{
			EstadoDAO dao = new EstadoDAO();
			estados = dao.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os estados.");
			e.printStackTrace();
		}	
	}
	
	public void salvar(){
		try{
			CidadeDAO dao = new CidadeDAO();
			dao.merge(cidade);
			novo();
			listar();
			Messages.addGlobalInfo("Cidade salva com sucesso !");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar a cidade");
			e.printStackTrace();
		}
	}
	
	public void excluir(ActionEvent evento){
		cidade = (Cidade) evento.getComponent().getAttributes().get("cidadeSelecionada");
		try{
			CidadeDAO dao = new CidadeDAO();
			dao.excluir(cidade);
			listar();
			Messages.addGlobalInfo("Cidade excluida com sucesso !");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar excluir a cidade");
			e.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		cidade = (Cidade) evento.getComponent().getAttributes().get("cidadeSelecionada");
		try{
			EstadoDAO dao = new EstadoDAO();
			estados = dao.listar("nome");
		}catch(RuntimeException e){
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os estados.");
			e.printStackTrace();
		}
	}
	
	public List<Cidade> getCidades() {
		return cidades;
	}
	
	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
	
	public Cidade getCidade() {
		return cidade;
	}
	
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	public List<Estado> getEstados() {
		return estados;
	}
	
	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

}
