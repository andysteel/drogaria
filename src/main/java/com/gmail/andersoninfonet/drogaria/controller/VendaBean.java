package com.gmail.andersoninfonet.drogaria.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import com.gmail.andersoninfonet.drogaria.dao.ClienteDAO;
import com.gmail.andersoninfonet.drogaria.dao.FuncionarioDAO;
import com.gmail.andersoninfonet.drogaria.dao.ProdutoDAO;
import com.gmail.andersoninfonet.drogaria.dao.VendaDAO;
import com.gmail.andersoninfonet.drogaria.model.Cliente;
import com.gmail.andersoninfonet.drogaria.model.Funcionario;
import com.gmail.andersoninfonet.drogaria.model.ItemVenda;
import com.gmail.andersoninfonet.drogaria.model.Produto;
import com.gmail.andersoninfonet.drogaria.model.Venda;

@ManagedBean
@ViewScoped
public class VendaBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Venda venda;
	private List<Produto> produtos;
	private List<ItemVenda> itensVenda;
	private List<Cliente> clientes;
	private List<Funcionario> funcionarios;
	private List<Venda> vendas;
	
	
	public void novo() {
		try {
			venda = new Venda();
			venda.setPrecoTotal(new BigDecimal("0.00"));
			
			ProdutoDAO produtoDAO = new ProdutoDAO();
			produtos = produtoDAO.listar("descricao");
			
			itensVenda = new ArrayList<ItemVenda>();
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar carrega a tela de vendas !!");
			erro.printStackTrace();
		}
	}
	
	public void listar(){
		try {
			VendaDAO vendaDAO = new VendaDAO();
			vendas = vendaDAO.listar("horario");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar as vendas !!");
			erro.printStackTrace();
		}
	}
	
	public void adicionar(ActionEvent evento){
		
		Produto produto = (Produto) evento.getComponent().getAttributes().get("produtoSelecionado");
		
		int achou = -1;
		for(int i=0;i<itensVenda.size();i++){
			if(itensVenda.get(i).getProduto().equals(produto)){
				achou = i;
			}
		}
		
		if(achou < 0){
			ItemVenda itemVenda = new ItemVenda();
			itemVenda.setProduto(produto);
			itemVenda.setPrecoParcial(produto.getPreco());
			itemVenda.setQuantidade(new Short("1"));
			
			itensVenda.add(itemVenda);
		}else{
			ItemVenda itemVenda = itensVenda.get(achou);
			itemVenda.setQuantidade(new Short(itemVenda.getQuantidade() + 1 +""));
			itemVenda.setPrecoParcial(produto.getPreco().multiply(new BigDecimal(itemVenda.getQuantidade())));
		}
		
		calcular();
		
	}
	
	public void remover(ActionEvent evento){
		ItemVenda itemVenda = (ItemVenda) evento.getComponent().getAttributes().get("itemSelecionado");
		
		int achou = -1;
		for(int i=0;i<itensVenda.size();i++){
			if(itensVenda.get(i).getProduto().equals(itemVenda.getProduto())){
				achou = i;
			}
		}
		
		if(achou > -1){
			itensVenda.remove(achou);
		}
		
		calcular();
	}
	
	public void calcular(){
		venda.setPrecoTotal(new BigDecimal("0.00"));
		for(int i=0;i<itensVenda.size();i++){
			ItemVenda itemVenda = itensVenda.get(i);
			
			venda.setPrecoTotal(venda.getPrecoTotal().add(itemVenda.getPrecoParcial()));
		}
	}
	
	public void atualizarPrecoParcial(){
		for(ItemVenda itemVenda : itensVenda){
			itemVenda.setPrecoParcial(itemVenda.getProduto().getPreco().multiply(new BigDecimal(itemVenda.getQuantidade())));
		}
		
		calcular();
	}
	
	public void finalizar(){
		try{
			venda.setFuncionario(null);
			venda.setCliente(null);
			
			FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
			funcionarios = funcionarioDAO.listarOrdenado();
			
			ClienteDAO clienteDAO = new ClienteDAO();
			clientes = clienteDAO.listarOrdenado();
			
			venda.setHorario(new Date());
			
		}catch(RuntimeException erro){
			Messages.addGlobalError("Ocorreu um erro ao tentar finalizar compra !");
			erro.printStackTrace();
		}
	}
	
	public void salvar(){
		try{
			if(venda.getPrecoTotal().signum() == 0){
				Messages.addGlobalError("Informe pelo menos um item para a venda !");
				return;
			}
			
			VendaDAO vendaDAO = new VendaDAO();
			vendaDAO.salvar(venda, itensVenda);
			novo();
			
			Messages.addGlobalInfo("Venda salva com sucesso");
		}catch(RuntimeException erro){
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar a venda !");
			erro.printStackTrace();
		}
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}
	
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	
	public List<ItemVenda> getItensVenda() {
		return itensVenda;
	}
	
	public void setItensVenda(List<ItemVenda> itensVenda) {
		this.itensVenda = itensVenda;
	}
	
	public Venda getVenda() {
		return venda;
	}
	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public List<Cliente> getClientes() {
		return clientes;
	}
	
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	public List<Venda> getVendas() {
		return vendas;
	}
}
