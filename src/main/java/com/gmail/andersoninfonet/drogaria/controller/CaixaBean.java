package com.gmail.andersoninfonet.drogaria.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Messages;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import com.gmail.andersoninfonet.drogaria.dao.CaixaDAO;
import com.gmail.andersoninfonet.drogaria.dao.FuncionarioDAO;
import com.gmail.andersoninfonet.drogaria.model.Caixa;
import com.gmail.andersoninfonet.drogaria.model.Funcionario;

@ManagedBean
@ViewScoped
public class CaixaBean {

	private ScheduleModel datas;
	private Caixa caixa;
	private CaixaDAO caixaDAO;
	private List<Funcionario> funcionarios;
	private FuncionarioDAO funcionarioDAO;
	private List<Caixa> caixas;
	
	@PostConstruct
	public void listar(){
		datas = new DefaultScheduleModel();	
		caixaDAO = new CaixaDAO();
		try{
			caixas = caixaDAO.listar();
		}catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar as datas");
			erro.printStackTrace();
		}
		if(caixas.size()>0){
			for(Caixa caixa : caixas){
				datas.addEvent(new DefaultScheduleEvent("Abertura de caixa - funcionario: "+caixa.getFuncionario().getPessoa().getNome(), caixa.getDataAbertura(), null));
			}
		}	
	}
	
	public void novo(SelectEvent evento){
		caixa = new Caixa();
		caixa.setDataAbertura((Date)evento.getObject());
		listarFuncionario();
	}
	
	public void listarFuncionario(){
		try {
			funcionarioDAO = new FuncionarioDAO();
			funcionarios = funcionarioDAO.listar();
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os funcionarios");
			erro.printStackTrace();
		}
	}
	
	public void salvar(){
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(caixa.getDataAbertura());
			calendar.add(Calendar.DATE, 1);
			caixa.setDataAbertura(calendar.getTime());
			
			caixaDAO = new CaixaDAO();
			caixaDAO.merge(caixa);
			Messages.addGlobalInfo("Caixa salvo com sucesso !");
			listar();
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar abertura de caixa");
			erro.printStackTrace();
		}
	}
	
	public ScheduleModel getdatas() {
		return datas;
	}
	
	public void setdatas(ScheduleModel datas) {
		this.datas = datas;
	}
	
	public Caixa getCaixa() {
		return caixa;
	}
	
	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}
	
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
}
