package com.gmail.andersoninfonet.drogaria.webservices;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.gmail.andersoninfonet.drogaria.dao.FabricanteDAO;
import com.gmail.andersoninfonet.drogaria.model.Fabricante;
import com.google.gson.Gson;

@Path("fabricante")
public class FabricanteService {

	@GET
	public String listar(){
		FabricanteDAO dao = new FabricanteDAO();
		List<Fabricante> fabricantes = dao.listar("descricao");
		
		Gson gson = new Gson();
		String json = gson.toJson(fabricantes);
		return json;
	}
	
	@GET
	@Path("{codigo}")
	public String buscar(@PathParam("codigo") Long codigo){
		FabricanteDAO dao = new FabricanteDAO();
		Fabricante fabricante = dao.buscar(codigo);
		
		Gson gson = new Gson();
		String json = gson.toJson(fabricante);
		return json;
	}
	
	@POST
	public String salvar(String json){
		Gson gson = new Gson();
		Fabricante fabricante =  gson.fromJson(json, Fabricante.class);
		
		FabricanteDAO dao = new FabricanteDAO();
		dao.salvar(fabricante);
		
		String jsonSaida = gson.toJson(fabricante);
		return jsonSaida;
	}
	
	@PUT
	public String editar(String json){
		Gson gson = new Gson();
		Fabricante fabricante =  gson.fromJson(json, Fabricante.class);
		
		FabricanteDAO dao = new FabricanteDAO();
		dao.editar(fabricante);
		
		String jsonSaida = gson.toJson(fabricante);
		return jsonSaida;
	}
	
	@DELETE
	@Path("{codigo}")
	public String excluir(@PathParam("codigo") Long codigo){
		
		
		FabricanteDAO dao = new FabricanteDAO();
		Fabricante fabricante = dao.buscar(codigo);
		dao.excluir(fabricante);
		
		Gson gson = new Gson();
		String jsonSaida = gson.toJson(fabricante);
		return jsonSaida;
	}
}
