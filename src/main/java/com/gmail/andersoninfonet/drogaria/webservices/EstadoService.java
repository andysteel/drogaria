package com.gmail.andersoninfonet.drogaria.webservices;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.gmail.andersoninfonet.drogaria.dao.EstadoDAO;
import com.gmail.andersoninfonet.drogaria.model.Estado;
import com.google.gson.Gson;

@Path("estado")
public class EstadoService {

	@GET
	public String listar(){
		EstadoDAO dao = new EstadoDAO();
		List<Estado> estados = dao.listar("nome");
		
		Gson gson = new Gson();
		String json = gson.toJson(estados);
		return json;
	}
}
