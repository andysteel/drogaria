package com.gmail.andersoninfonet.drogaria.webservices;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.gmail.andersoninfonet.drogaria.dao.CidadeDAO;
import com.gmail.andersoninfonet.drogaria.model.Cidade;
import com.google.gson.Gson;

@Path("cidade")
public class CidadeService {

	@GET
	@Path("{estadoCodigo}")
	public String listar(@PathParam("estadoCodigo") Long estadoCodigo){
		CidadeDAO dao = new CidadeDAO();
		List<Cidade> cidades = dao.buscarPorEstado(estadoCodigo);
		
		Gson gson = new Gson();
		String json = gson.toJson(cidades);
		return json;
	}
}
