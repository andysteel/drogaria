package com.gmail.andersoninfonet.drogaria.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name="movimentacao", schema="admin")
public class Movimentacao extends GenericModel{

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date horario;
	
	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal valor;
	
	@ManyToOne
	@JoinColumn(nullable=false)
	private Caixa caixa;
	

	public Date getHorario() {
		return horario;
	}

	public void setHorario(Date horario) {
		this.horario = horario;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}
}
