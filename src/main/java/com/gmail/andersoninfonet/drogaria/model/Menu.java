package com.gmail.andersoninfonet.drogaria.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@SuppressWarnings("serial")
@Entity
@Table(name="menu", schema="admin")
public class Menu extends GenericModel{

	@Column(length = 50, nullable = false)
	private String label;
	
	@Column(length = 80)
	private String caminho;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(referencedColumnName = "codigo")
	@Fetch(FetchMode.SUBSELECT)
	private List<Menu> menus;
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getCaminho() {
		return caminho;
	}
	
	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}
	
	public List<Menu> getMenus() {
		return menus;
	}
	
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
}
