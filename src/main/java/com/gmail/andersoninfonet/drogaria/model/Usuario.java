package com.gmail.andersoninfonet.drogaria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gmail.andersoninfonet.drogaria.enumm.TipoUsuario;


@SuppressWarnings("serial")
@Entity
@Table(name="usuario", schema="admin")
public class Usuario extends GenericModel{

	@Column(length=32, nullable=false)
	private String senha;
	
	@Transient
	private String senhaSemCriptografia; 
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length=30)
	private TipoUsuario tipoUsuario;
	
	@Column(nullable=false)
	private Boolean ativo;
	
	@OneToOne
	@JoinColumn(nullable=false)
	private Pessoa pessoa;
	

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getSenhaSemCriptografia() {
		return senhaSemCriptografia;
	}
	
	public void setSenhaSemCriptografia(String senhaSemCriptografia) {
		this.senhaSemCriptografia = senhaSemCriptografia;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}
	
	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Transient
	public String getAtivoFormatado(){
		String ativoFormatado = "NÃO";
		if(ativo){
			ativoFormatado = "SIM";
		}
		return ativoFormatado;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
}
