package com.gmail.andersoninfonet.drogaria.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@SuppressWarnings("serial")
@Entity
@Table(name="cliente", schema="admin")
public class Cliente extends GenericModel{

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date dataCadastro;
	
	@Column(nullable=false)
	private Boolean liberado;
	
	@OneToOne
	@JoinColumn(nullable=false)
	Pessoa pessoa;
	

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Boolean getLiberado() {
		return liberado;
	}

	public void setLiberado(Boolean liberado) {
		this.liberado = liberado;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
}
