package com.gmail.andersoninfonet.drogaria.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.gmail.andersoninfonet.drogaria.model.ItemVenda;
import com.gmail.andersoninfonet.drogaria.model.Produto;
import com.gmail.andersoninfonet.drogaria.model.Venda;
import com.gmail.andersoninfonet.drogaria.util.HibernateUtil;

public class VendaDAO extends GenericDAO<Venda>{

	public void salvar(Venda venda, List<ItemVenda> itensVenda){
		Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		Transaction transacao = null;
		
		try{
			transacao = sessao.beginTransaction();
			sessao.save(venda);
			for(ItemVenda itemVenda : itensVenda){
				itemVenda.setVenda(venda);
				
				sessao.save(itemVenda);
				
				Produto produto = itemVenda.getProduto();
				int quantidade = produto.getQuantidade() - itemVenda.getQuantidade();
				if(quantidade >= 0){
					produto.setQuantidade(new Short( quantidade + ""));
					sessao.update(produto);
				}else{
					throw new RuntimeException("Quantidade insuficiente em estoque !!");
				}
				
			}
			transacao.commit();
			
		}catch(RuntimeException e){
			if(transacao != null) transacao.rollback();
			throw e;
		}finally {
			sessao.close();
		}
	}
}
